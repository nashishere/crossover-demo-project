'use strict';

var application = angular.module('CrossoverDemoProjectApp');

application.controller('MainCtrl', ['$scope', '$timeout', function ($scope, $timeout) {
	var firebase = new Firebase('https://crossoverdemoproject.firebaseio.com');
	firebase.on('value', function(snapshot) {
		$timeout(function() {
			$scope.database = snapshot.val();
		});
	});

	$scope.chartOptions = {
		size: {
			width: 100,
			height: 100
		}
	};

	$scope.getStyleForPercentageBox = function(name, value) {
		var width = 0;
		switch (name) {
			case 'metrics':
			case 'build':
				width = value.progress;
				break;

			case 'unit_test':
			case 'functional_test':
				width = value.coverage;
				break;
		}

		if (width <= 0) {
			return { 'width': '100%', 'background-color': '#bf010a' };
		} else if (width >= 100) {
			return { 'width': '100%', 'background-color': '#538136' };
		} else {
			return { 'width': width + '%', 'background-color': 'auto' };
		}
	};

	$scope.getClassForDetailBox = function(name, value) {
		var width = 0;
		switch (name) {
			case 'metrics':
			case 'build':
				width = value.progress;
				break;

			case 'unit_test':
			case 'functional_test':
				width = value.coverage;
				break;
		}

		if (width <= 0) {
			return 'code-red';
		} else {
			return 'code-green';
		}
	};

	$scope.currentlyShowing = null;

	$scope.showDetails = function(value) {
		$scope.currentlyShowing = $scope.currentlyShowing === value ? null : value;

		$timeout(function() {
			window.dispatchEvent(new Event('resize'));
		});
	};

	$scope.isShowing = function(value) {
		return (!!value) && (!!$scope.currentlyShowing) && ($scope.currentlyShowing.id === value.id);
	};
}]);

application.filter('capitalize', function() {
	return function(input) {
		return (!!input) ? String(input).charAt(0).toUpperCase() + String(input).substr(1).toLowerCase() : '';
	}
});

application.filter('withTrendArrows', function() {
	return function(input) {
		if (!!input) {
			if (input.to - input.from < 0) {
				return 'trend-down';
			} else if (input.to - input.from > 0) {
				return 'trend-up';
			} else {
				return 'trend-same';
			}
		} else {
			return '';
		}
	}
});
