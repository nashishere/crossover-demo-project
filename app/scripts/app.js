'use strict';

angular.module('CrossoverDemoProjectApp', [
	'ngAnimate',
	'ngSanitize',
	'chart.js',
	'firebase'
]);
