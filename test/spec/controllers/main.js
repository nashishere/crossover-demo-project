'use strict';

describe('Crossover Demo Application', function () {

	// load the controller's module
	beforeEach(module('CrossoverDemoProjectApp'));

	var scope, filter;

	// Initialize the controller and a mock scope
	beforeEach(inject(function ($controller, $rootScope, $filter) {
		scope = $rootScope.$new();
		filter = $filter;

		$controller('MainCtrl', { $scope: scope, $filter: filter });
	}));

	it('should capitalize the first letters of strings', inject(function () {
		expect(filter('capitalize')('foo')).toBe('Foo');
		expect(filter('capitalize')('bar')).toBe('Bar');
		expect(filter('capitalize')('')).toBe('');
		expect(filter('capitalize')('foo bar')).toBe('Foo bar');
		expect(filter('capitalize')(null)).toBe('');
		expect(filter('capitalize')(undefined)).toBe('');
		expect(filter('capitalize')(123)).toBe('123');
	}));

	it('should write the css class name for trend arrows', inject(function () {
		expect(filter('withTrendArrows')({ from: 10, to: 11 })).toBe('trend-up');
		expect(filter('withTrendArrows')({ from: 22, to: 10 })).toBe('trend-down');
		expect(filter('withTrendArrows')({ from: 30, to: 30 })).toBe('trend-same');
	}));

	it('should work as expected', inject(function () {
		var database = {
			"nodes": [
				{
					"id": 1,
					"changelist": null,
					"build": "Tentrox-R1_1235",
					"owner": "sven",
					"startedAt": "2014-04-18T12:14:03.547Z",
					"state": "pending",
					"operations": {
						"metrics": {
							"progress": 80,
							"test": {
								"from": 64,
								"to": 64
							},
							"maintainability": {
								"from": 53,
								"to": 55
							},
							"security": {
								"from": 33,
								"to": 30
							},
							"workmanship": {
								"from": 72,
								"to": 70
							}
						},
						"build": {
							"state": "Pending",
							"startedAt": "2014-04-18T12:14:03.547Z",
							"progress": 0
						},
						"unit_test": {
							"success": 80,
							"failed": 20,
							"coverage": 50,
							"progress": 20
						},
						"functional_test": {
							"success": 60,
							"failed": 30,
							"coverage": 15,
							"progress": 33
						}
					}
				},
				{
					"id": 2,
					"changelist": "147865",
					"build": null,
					"owner": "mortred",
					"startedAt": "2014-04-17T12:19:08.147Z",
					"state": "accepted",
					"operations": {
						"metrics": {
							"progress": 80,
							"test": {
								"from": 64,
								"to": 64
							},
							"maintainability": {
								"from": 53,
								"to": 55
							},
							"security": {
								"from": 33,
								"to": 30
							},
							"workmanship": {
								"from": 72,
								"to": 70
							}
						},
						"build": {
							"state": "Pending",
							"startedAt": "2014-04-18T12:14:03.547Z",
							"progress": 0
						},
						"unit_test": {
							"success": 80,
							"failed": 20,
							"coverage": 50,
							"progress": 20
						},
						"functional_test": {
							"success": 60,
							"failed": 30,
							"coverage": 15,
							"progress": 33
						}
					}
				}
			]
		};

		// Currently selected data must be null
		expect(scope.currentlyShowing).toBe(null);

		// Selecting the second row
		scope.showDetails(database.nodes[1]);

		// Currently selected data must be the database's second element
		expect(scope.currentlyShowing).toBe(database.nodes[1]);
		expect(scope.isShowing(database.nodes[0])).toBe(false);
		expect(scope.isShowing(database.nodes[1])).toBe(true);
		expect(scope.isShowing(null)).toBe(false);
		expect(scope.isShowing(undefined)).toBe(false);

		// Selecting the second row again, so the selected row must be clear now
		scope.showDetails(database.nodes[1]);

		// Currently selected data must be null
		expect(scope.currentlyShowing).toBe(null);
		expect(scope.isShowing(database.nodes[1])).toBe(false);
	}));
});
