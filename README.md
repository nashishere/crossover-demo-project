# Crossover Demo Project

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Prerequisites

Install `node` and `npm`.  
Install `grunt` with the `npm install -g grunt-cli` command.  
Install `bower` with the `npm install -g bower` command.  

## Build & development

Run `grunt build` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.  

## Complexity Report

Running `grunt plato:complexity` will run the complexity check with plato.  
You can see the reports in the `./report` folder after the task is complete.
